import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import routes from './routes/index.mjs';
dotenv.config();

const app = express();
const port = process.env.PORT || 5000;
const { json } = bodyParser;

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/', express.static('client/build'));

app.use(json());

app.use('/api', routes);

// Error handlers
app.use((err, req, res, next) => {
  const errorStatus = err.status || 500;
  res.status(errorStatus);
  res.send({
    error: {
      status: errorStatus,
      message: err.message,
    },
  });
});

// Server
app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
