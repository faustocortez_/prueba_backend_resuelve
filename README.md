# Prueba Backend Ingenieria Resuelve

_Api desarrollada con node y express para la resolución de la prueba backend_

### Requisitos 📋

* node v16.8.0
* npm 7.21.0

## Empezando 🚀

_Clona el repositorio del proyecto_
```
  git clone git@gitlab.com:faustocortez_/prueba_backend_resuelve.git
```
### Configuración 🔧

_Renombrar el archivo example.env a .env_

_Para ejecutar el proyecto con nodejs_

* Instalar dependencias del proyecto

```
  npm install
```

* El proyecto usa nodemon, este paquete le permitira reiniciar el servidor cada vez que detecte un cambio en los archivos. De igual manera la instrucción inicia la vista de la aplicación.
```
  npm run dev
```

* El servidor se levanta en el puerto 4000
```
  http://localhost:4000
```

* El cliente se levanta en el puerto 3000
```
  http://localhost:3000
```

## Consultando los endpoints ⚙️

_El proyecto tiene un endpoint que recibe el json con los datos para calcular el sueldo completo de los jugadores_

* Realice un post a la siguiente url

```
  <!-- LOCAL -->
  POST http://localhost:4000/api/sueldo

  <!-- Heroku server -->
  POST https://prueba-backend-resuelve.herokuapp.com/api/sueldo
```
* La API recibe un JSON con la estructura:

```json
// ejmplo del body
{
   "jugadores" : [  
      {  
        "nombre": "Luis",
        "nivel": {
          "Cuauh": 20
        },
        "goles": 19,
        "sueldo": 50000,
        "bono": 10000,
        "sueldo_completo":null,
        "equipo": "rojo"
      },
      .
      .
      .
   ]
}
```
* Respuesta de la API:

```json
{
   "jugadores": [
      {
        "nombre": "Luis",
        "goles_minimos": 20,
        "goles": 19,
        "sueldo": 50000,
        "bono": 10000,
        "bono_alcanzado": 9050,
        "sueldo_completo": 59050,
        "equipo": "rojo"
      },
      .
      .
   ]
}
```

## Autor ✒️

* **Fausto A. Cortez Cardoz** - *Frontend Developer (con ganas de ser Backend)* - [faustocortez_](https://gitlab.com/faustocortez_)