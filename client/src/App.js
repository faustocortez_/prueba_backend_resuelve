import React, { useState, Fragment } from 'react';
import logo from './resuelve.png'
import CodeEditor from '@uiw/react-textarea-code-editor';
import axios from 'axios';


function App() {
  const resuelveFCPlayers = `{
    "jugadores" : [  
      {
        "nombre": "Juan",
        "nivel": {
          "A": 5
        },
        "goles": 6,
        "sueldo": 40000,
        "bono": 15000,
        "sueldo_completo": null,
        "equipo": "azul"
      },
      {  
          "nombre":"Pedro",
          "nivel": {
            "B": 10
          },
          "goles": 7,
          "sueldo": 20000,
          "bono": 5000,
          "sueldo_completo":null,
          "equipo":"rojo"
      },
      {  
          "nombre":"Martín",
          "nivel": {
            "C": 15
          },
          "goles": 16,
          "sueldo":25500,
          "bono": 8000,
          "sueldo_completo":null,
          "equipo":"azul"
      },
      {  
          "nombre":"Luis",
          "nivel": {
            "Cuauh": 20
          },
          "goles": 19,
          "sueldo": 50000,
          "bono": 10000,
          "sueldo_completo":null,
          "equipo":"rojo"

      }
    ]
  }`;
  const [code, setCode] = React.useState(resuelveFCPlayers);

  const [players, setPlayers] = useState(null);

  const playersPreText = `{
    "jugadores" : [  
      {  
        "nombre":"Luis",
        "nivel": {
          "Cuauh": 20
        },
        "goles": 19,
        "sueldo": 50000,
        "bono": 10000,
        "sueldo_completo":null,
        "equipo":"resuelveFC"

    },
      .
      .
      // resto de los jugadores
    }`;

    const levelPreTExt = `
    "nivel": {
      "Principiante": 10,
      "L. Messi": 20,
      "C. Ronaldo": 30,
      .
      .
      // más niveles --> "nombrenivel": [int] minimo de goles
    },`;

  const calculateSalary = async (players) => {
    const body = JSON.parse(players);    
    const data = await axios.post('http://localhost:4000/api/sueldo', body);
    setPlayers(data.data.jugadores);
  }

  return (
    <div className="app-container">
      <div className="app-header">
        <h1>
          Prueba backend Resuelve
        </h1>
        <figure>
          <img src={logo} alt="logo"></img>
        </figure>
      </div>

      <div className="app-content">
        <div className="app-content__left">
          <h2 className="app-content__subtitle">
            Editor JS
          </h2>
          <div className="app-code-editor">
            <CodeEditor
              value={code}
              language="js"
              placeholder="// Please enter JS code..."
              onChange={(evn) => setCode(evn.target.value)}
              style={{
                overflow: 'auto',
                height: '78vh',
                fontSize: '1rem',
                backgroundColor: '#f8f8f8',
              }}
            />

            <button
              disabled={code === ''}
              className="app-code-editor__request-button"
              onClick={() => calculateSalary(code)}
            >
              Calcular sueldo completo
            </button>
          </div>
        </div>

        <div className="app-content__right">
          { players && (
            <Fragment>
              <h2 className="app-content__subtitle --bshadow">
                Respuesta de la API
              </h2>
              <div className="app-response">
                <div className="app-response__data">
                  { players.map((player) => 
                    <details open key={player.nombre}>
                      <summary>
                        { player.nombre } | { player.equipo.toUpperCase() }
                      </summary>
                      <ul>
                        <li>
                          <strong>Mínimo de goles: </strong>
                          { player.goles_minimos }
                        </li>
                        <li>
                          <strong>Goles: </strong>
                          { player.goles }
                        </li>
                        <li>
                          <strong>Bono: </strong>
                          ${ player.bono }
                        </li>
                        <li>
                          <strong>Bono alcanzado: </strong>
                          ${ player.bono_alcanzado }
                        </li>
                        <li>
                          <strong>Sueldo: </strong>
                          ${ player.sueldo }
                        </li>
                        <li>
                          <strong>Sueldo completo: </strong>
                          ${ player.sueldo_completo }
                        </li>
                      </ul>
                    </details>
                  )}
                </div>

                <div className="app-response__json">
                  <pre>
                    { JSON.stringify({ jugadores: players }, null, 2) }
                  </pre>
                </div>
              </div>
            </Fragment>
          )}

          { players === null && (
            <div className="app-empty">
              <p className="app-empty__instructions">
                Para calcular los sueldos de los jugadores sólo agrega el JSON con la estructura:
                <pre>
                  {playersPreText}
                </pre>
              </p>

              <p className="app-empty__instructions">
                [ BONUS ] - Para calcular el sueldo de los jugadores de otros equipos con distintos mínimos por nivel,
                basta con modificar la clave (key) <code>"nivel"</code> con la siguiente estructura:
                <pre>
                  {levelPreTExt}
                </pre>

                <br/>
                De manera predeterminada los el "Editor JS" contiene el JSON para probar la
                la API con los nombres de los jugadores de Resuelve FC.
              </p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
