const mapearEquiposNiveles = (jugadores) => {
  let equipos = [];
  const tablaNiveles = new Map();

  jugadores.forEach((jugador) => {
    if (!equipos.includes(jugador.equipo)) {
      equipos = [...equipos, jugador.equipo];
    }
    if (tablaNiveles.get(jugador.nivel.nombre) === undefined) {
      for (const [key, value] of Object.entries(jugador.nivel)) {
        tablaNiveles.set(key, value);
      }
    }
  });
  console.table(tablaNiveles);

  return {
    equipos,
    tablaNiveles,
  };
};

const formarEquipos = (equipos, jugadores) => {
  const equiposFormados = {};
  equipos.forEach((equipo) => {
    equiposFormados[equipo] = jugadores.filter((jugador) => jugador.equipo === equipo);
  });

  return equiposFormados;
};

const calcularBonoSueldo = (equipos, niveles) => {
  const responseData = { jugadores: [] };

  Object.entries(equipos).forEach(([equipo, jugadoresEquipo]) => {
    // calculo de goles
    const totalGoles = jugadoresEquipo.reduce((prev, current) => ({ goles: prev.goles + current.goles }));
    const minimo = jugadoresEquipo
      .reduce((prev, current) => ({ cantidad: Object.values(prev.nivel)[0] + Object.values(current.nivel)[0] }));
    console.log(`\n* Meta equipo ${equipo}: ${totalGoles.goles} / ${minimo.cantidad}`);
    const alcancePorEquipo = totalGoles.goles < minimo.cantidad
      ? parseInt(((totalGoles.goles / minimo.cantidad) * 100).toFixed(2), 10)
      : 100;

    // calcular bono por jugador
    jugadoresEquipo.forEach((jugador) => {
      const nivel = Object.keys(jugador.nivel)[0];
      const alcanceIndividual = jugador.goles < niveles.get(nivel)
        ? parseInt(((jugador.goles / niveles.get(nivel)) * 100).toFixed(2), 10)
        : 100;
      const alcanceTotal = (alcanceIndividual + alcancePorEquipo) / 2;
      const bonoAlcanzado = (alcanceTotal * jugador.bono) / 100;

      // calcular sueldo final
      const sueldoCompleto = jugador.sueldo + bonoAlcanzado;

      const data = {
        nombre: jugador.nombre,
        goles_minimos: niveles.get(nivel),
        goles: jugador.goles,
        sueldo: jugador.sueldo,
        bono: jugador.bono,
        bono_alcanzado: bonoAlcanzado,
        sueldo_completo: sueldoCompleto,
        equipo,
      };

      responseData.jugadores = [...responseData.jugadores, data];

      console.log(`\n${jugador.nombre}`);
      console.log(`---- nivel: ${nivel}`);
      console.log(`---- mínimo de goles: ${minimo.cantidad}`);
      console.log(`---- alcance individual: ${alcanceIndividual}%`);
      console.log(`---- alcance en equipo: ${alcancePorEquipo}%`);
      console.log(`---- alcance total: ${alcanceTotal}%`);
      console.log(`---- bono: ${jugador.bono}`);
      console.log(`---- sueldo fijo: ${jugador.sueldo}`);
      console.log(`---- bono alcanzado: ${bonoAlcanzado}`);
      console.log(`---- sueldo completo: ${sueldoCompleto}`);
    });
  });

  return responseData;
};

const calcularSueldoCompleto = (req, res, next) => {
  try {
    // Obetener jugadores
    const jugadores = req.body?.jugadores;
    // Mapear equipos y niveles
    const { equipos, tablaNiveles } = mapearEquiposNiveles(jugadores);

    // Formar equipos
    const equiposFormados = formarEquipos(equipos, jugadores);

    // Calcular bono más sueldo
    const sueldoCompleto = calcularBonoSueldo(equiposFormados, tablaNiveles);
    return res.json(sueldoCompleto);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

export default calcularSueldoCompleto;
