import express from 'express';
import calcularSueldoCompleto from '../controllers/calcularSueldoCompleto.mjs';

const router = express.Router();

router.get('/', (req, res) => {
  res.json({ greetings: 'Haloooo!' });
});

router.post('/sueldo', calcularSueldoCompleto);

export default router;
